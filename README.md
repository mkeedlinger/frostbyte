# FrostByte
This is a simple game (top down shooter) written in javascript served by node.js.

We do our best to comply with the [Semver](http://semver.org/) versioning system.

### About to merge to the dev branch?
Did you...
- Mark the task(s) as complete on Asana?
- Add the changes to changelog.html?
- Document any bugs created or fixed as a result of your changes in our Issues?

### Resources
Dev3 FrostByte wiki:  [Home](http://gitlab.mke8.me/dev3/frostbyte/wikis/home)  |  [Installing/Using](http://gitlab.mke8.me/dev3/frostbyte/wikis/How-to-install)

A lot of the time they're down, but you can [play on our servers!](http://frostbyte.mke8.me/)

Installing node.js: [node.js Wiki](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager)